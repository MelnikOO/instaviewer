//
//  Comment.h
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject

@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *text;

@end
