//
//  AuthorizeViewController.m
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "AuthorizeViewController.h"

static NSString *instagramAuthorizeURI = @"https://api.instagram.com/oauth/authorize/";

@interface AuthorizeViewController ()

@end

@implementation AuthorizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Login";
    _webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    self.view = _webView;
    _webView.delegate = self;
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:
                                                        [NSString stringWithFormat:
                                                         @"%@?client_id=%@&redirect_uri=%@&response_type=token", instagramAuthorizeURI, kInstagramClientId, kInstagramRedirectURI]]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *currentURL = webView.request.URL.absoluteString;
    NSString *match = [NSString stringWithFormat:@"%@#access_token=", kInstagramRedirectURI];
    if ([currentURL hasPrefix:match]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kGotAccessID object:[currentURL stringByReplacingOccurrencesOfString:match withString:@""]];
    }
}

@end
