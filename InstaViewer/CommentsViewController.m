//
//  CommentsViewController.m
//  InstaViewer
//
//  Created by Олег Мельник on 16.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "CommentsViewController.h"
#import "Comment.h"

@interface CommentsViewController () {
    UIBarButtonItem *doneButtonItem;
}

@end

@implementation CommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(performDismiss)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)performDismiss {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _media.comments.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    Comment *comment = _media.comments[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@", comment.username, comment.text];
    return cell;
}

@end
