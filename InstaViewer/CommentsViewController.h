//
//  CommentsViewController.h
//  InstaViewer
//
//  Created by Олег Мельник on 16.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Media.h"

#pragma mark - Display top comments received by request of recent

@interface CommentsViewController : UITableViewController

@property (strong, nonatomic) Media *media;

@end
