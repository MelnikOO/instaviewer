//
//  AuthorizeViewController.h
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthorizeViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *webView;

@end
