//
//  Media.h
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Media : NSObject

#pragma mark - Media object

@property (strong, nonatomic) NSString *photoURL;
@property (strong, nonatomic) NSNumber *commentsCount;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *photoID;
@property (strong, nonatomic) NSArray *likes;
@property (strong, nonatomic) NSNumber *likesCount;
@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) NSArray *comments;

@end
