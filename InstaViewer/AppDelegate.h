//
//  AppDelegate.h
//  InstaViewer
//
//  Created by Олег Мельник on 14.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *accessToken;

@end

