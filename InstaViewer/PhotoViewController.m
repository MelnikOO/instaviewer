//
//  PhotoViewController.m
//  InstaViewer
//
//  Created by Олег Мельник on 14.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "PhotoViewController.h"
#import "Like.h"
#import "CommentsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *mediaChanged = @"mediaChanged";

@interface PhotoViewController ()

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addObserver:self forKeyPath:@"media" options:NSKeyValueObservingOptionNew context:(__bridge void *)mediaChanged];
    _commentsLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _commentsLabel.font = [UIFont systemFontOfSize:50];
    _commentsLabel.textColor = [UIColor whiteColor];
    [_imageView addSubview:_commentsLabel];
    if (_media) {
        [self getImageFromMedia:_media];
    }
    
    // we want to open comments screen by tapping on photo
    UITapGestureRecognizer *gestureRecognizer =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentLastComments:)];
    _imageView.userInteractionEnabled = YES;
    [_imageView addGestureRecognizer:gestureRecognizer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// observe changing of media by KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([(__bridge id)context isKindOfClass:[NSString class]] && [(__bridge NSString *)context isEqualToString:mediaChanged]) {
        [self getImageFromMedia:_media];
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}



- (void)getImageFromMedia:(Media *)media {
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:media.photoID
                                                     done:^(UIImage *image, SDImageCacheType cacheType) { //get image from SDWebImage disk cache
                                                         if (!image) {
                                                             [[SDWebImageDownloader sharedDownloader]
                                                              downloadImageWithURL:[NSURL URLWithString:media.photoURL]
                                                              options:0
                                                              progress:nil
                                                              completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) { // download if it does not exist
                                                                  if (image && finished) {
                                                                      [[SDImageCache sharedImageCache] storeImage:image forKey:media.photoID toDisk:YES]; //save downloaded image to cache
                                                                      NSLog(@"image %@ downloaded and saved", media.photoID);
                                                                  }
                                                                  else {
                                                                      NSLog(@"downloading and saving failed: %@", error.localizedDescription);
                                                                  }
                                                                  _imageView.image = image;
                                                              }];
                                                         }
                                                         _imageView.image = image;
                                                     }];
    [self showCommentsCount:media];
    [self showLikesCount:media];

}

- (void)showCommentsCount:(Media *)media  {
    _commentsLabel.frame = CGRectMake(_imageView.center.x-80, _imageView.center.x-80, 70, 70);
    _commentsLabel.text = [NSString stringWithFormat:@"%ld", (long)[media.commentsCount integerValue]];
}



- (void)showLikesCount:(Media *)media {
    if (media.likesCount > 0) {
        NSMutableString *likesString = [NSMutableString stringWithString:@"Likes: "];
        for (Like *like in media.likes) {
            [likesString appendString:like.username];
            [likesString appendString:@", "];
        }
        if ([media.likesCount integerValue] > 4) { // instagram api gives only 4 first likers in request of recent
            [likesString appendString:[NSString stringWithFormat:@"and %ld more", [media.likesCount integerValue] - 4]];
        }
        _likedLabel.text = likesString;
    }
    else {
        _likedLabel.text = @"";
    }
    
}
                                          
- (void)presentLastComments:(id)sender{
    if (_media) {
        CommentsViewController *commentsController = [[CommentsViewController alloc] initWithStyle:UITableViewStylePlain];
        commentsController.media = _media;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:commentsController];
        commentsController.title = @"Last comments";
        [self presentViewController:navController animated:YES completion:nil];
    }
}

@end
