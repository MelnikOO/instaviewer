//
//  ViewController.m
//  InstaViewer
//
//  Created by Олег Мельник on 14.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "MainViewController.h"
#import "AuthorizeViewController.h"
#import "AppDelegate.h"
#import "PhotoViewController.h"
#import <SSKeychain/SSKeychain.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface MainViewController () {
    NSString *accessToken;
    AppDelegate *appDelegate;
    DataLoader *dataLoader;
    PhotoViewController *currentController;
    NSInteger currentPhotoIndex;
    NSArray *allMedia;
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation MainViewController

#pragma mark - Class methods -
#pragma mark - Initalization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self performAuthorize];
}


// Initialize UI
- (void)initUI {
    appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    dataLoader = [DataLoader sharedInstance];
    dataLoader.delegate = self;
    currentController = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    _prevButton.enabled = NO;
    currentController.view.frame = CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height-30);
    [self addChildViewController:currentController];
    [currentController didMoveToParentViewController:self];
    [self.view addSubview:currentController.view];
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = self.view.center;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

// Authorize
- (void)performAuthorize {
    accessToken = [SSKeychain passwordForService:@"Instagram API" account:@"ACCESS_KEY"];
    if (accessToken == nil) { // open web view with authorization page
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didGetAccessID:) name:kGotAccessID object:nil];
        
        // show screen with authorization webpage
        AuthorizeViewController *avc = [[AuthorizeViewController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc]
                                                 initWithRootViewController:avc];
        double delay = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
        __weak MainViewController *_self = self;
        dispatch_after(popTime, dispatch_get_main_queue(), ^{
            __strong MainViewController *__self = _self;
            [__self presentViewController:navController
                                 animated:YES
                               completion:nil];
            
        });
    }
    else {
        NSLog(@"accID: %@", accessToken);
        appDelegate.accessToken = accessToken;
        [dataLoader loadAllMedia];
    }
}

// Save access token to keychain
- (void)didGetAccessID:(NSNotification *)notification {
    NSString *accessID = notification.object;
    accessToken = accessID;
    appDelegate.accessToken = accessToken;
    NSLog(@"%@", accessID);
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGotAccessID object:nil];
    [dataLoader loadAllMedia];
    NSError *error = nil;
    if (![SSKeychain setPassword:accessID forService:@"Instagram API" account:@"ACCESS_KEY" error:&error]) {
        NSLog(@"Error saving accID to keychain: %@", error.localizedDescription);
    }
}

#pragma mark - DataLoaderDelegate

//received array of media from data loader

- (void)dataLoader:(DataLoader *)aDataLoader didFinishLoadingMedia:(NSArray *)mediaArray {
    [activityIndicator stopAnimating];
    allMedia = mediaArray;
    currentController.media = mediaArray[0]; // setup first photo to photo controller
}

//
- (IBAction)onPrevButtonTapped:(id)sender {
    if (currentController.imageView.image) {
        _prevButton.enabled = --currentPhotoIndex > 0 ? YES : NO;
        currentController.media = allMedia[currentPhotoIndex];
    }
}

- (IBAction)onNextButtonTapped:(id)sender {
    if (currentController.imageView.image) {
        _prevButton.enabled = ++currentPhotoIndex > 0 ? YES : NO;
        currentController.media = allMedia[currentPhotoIndex];
    }
    
}

// display share dialog
- (IBAction)onShareButtonTapped:(id)sender {
    if (currentController.imageView.image) {
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[currentController.imageView.image] applicationActivities:nil];
        [self presentViewController:activityViewController animated:YES completion:nil];
    }
}

@end
