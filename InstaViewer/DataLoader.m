//
//  DataLoader.m
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "DataLoader.h"
#import "Media.h"
#import "Like.h"
#import "Comment.h"
#import "InstagramResponse.h"
#import "AppDelegate.h"
#import <RestKit/RestKit.h>

static NSString *newNextUrl = @"newNextUrl";
static NSString *getRecentMedia = @"https://api.instagram.com/v1/users/self/media/recent/?min_timestamp=0&access_token=%@";

@implementation DataLoader {
    AppDelegate *appDelegate;
    NSMutableArray *allMedia;
    RKResponseDescriptor *responseDescriptor;
}

- (id)init {
    self = [super init];
    if (self) {
        appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
        allMedia = [NSMutableArray array];
        [self initRestKit];
        [self addObserver:self
               forKeyPath:@"nextURL"
                  options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                  context:(__bridge void *)(newNextUrl)];
    }
    return self;
}

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static DataLoader *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

// initialize RestKit object mapping
- (void)initRestKit {
    RKObjectMapping *likesMapping = [RKObjectMapping mappingForClass:[Like class]];
    [likesMapping addAttributeMappingsFromArray:@[@"username"]];
    RKObjectMapping *commentsMapping = [RKObjectMapping mappingForClass:[Comment class]];
    [commentsMapping addAttributeMappingsFromDictionary:@{
                                                       @"from.username": @"username",
                                                       @"text": @"text"
                                                       }];
    RKObjectMapping *mediaMapping = [RKObjectMapping mappingForClass:[Media class]];
    [mediaMapping addAttributeMappingsFromDictionary:@{
                                                       @"type": @"type",
                                                       @"comments.count": @"commentsCount",
                                                       @"images.standard_resolution.url": @"photoURL",
                                                       @"id": @"photoID",
                                                       @"caption.text": @"caption",
                                                       @"likes.count": @"likesCount"
                                                       }];
    
    // add relationship from likes to media
    [mediaMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"likes.data" toKeyPath:@"likes" withMapping:likesMapping]];
    [mediaMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"comments.data" toKeyPath:@"comments" withMapping:commentsMapping]];
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[InstagramResponse class]];
    [responseMapping addAttributeMappingsFromDictionary:@{
                                                          @"pagination.next_url": @"nextURL",
                                                          @"pagination.next_max_id": @"nextMaxID"
                                                          }];
    [responseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data"
                                                                                    toKeyPath:@"media"
                                                                                  withMapping:mediaMapping]];
    responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                      method:RKRequestMethodGET
                                                                 pathPattern:nil
                                                                     keyPath:nil
                                                                 statusCodes:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([(__bridge id)context isKindOfClass:[NSString class]] && [(__bridge NSString *)context isEqualToString:newNextUrl]) {
        // instagram has pagination in its requests. we keep observing next url property, and if it is not nil we load next page until the end
        if (self.nextURL) {
            [self getNextMedia];
        }
        else {
            NSLog(@"finished loading %ld media", allMedia.count);
            // sort results of request by comments count
            [allMedia sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                Media *media1 = obj1;
                Media *media2 = obj2;
                if ([media1.commentsCount integerValue] > [media2.commentsCount integerValue])
                    return NSOrderedAscending;
                else if ([media1.commentsCount integerValue] < [media2.commentsCount integerValue])
                    return NSOrderedDescending;
                return NSOrderedSame;
            }];
            if (_delegate && [_delegate respondsToSelector:@selector(dataLoader:didFinishLoadingMedia:)]) {
                [_delegate dataLoader:self didFinishLoadingMedia:[allMedia copy]];
                [allMedia removeAllObjects];
            }
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


- (void)getNextMedia {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.nextURL]];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                        responseDescriptors:@[responseDescriptor]];
    __weak DataLoader *_self = self;
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        __strong DataLoader *__self = _self;
        NSArray *resultArray = [mappingResult array];
        id resultObj = [resultArray firstObject];
        if ([resultObj isKindOfClass:[InstagramResponse class]]) {
            InstagramResponse *response = resultObj;
            [allMedia addObjectsFromArray:response.media];
            __self.nextURL = response.nextURL;
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"failure loading media: %@", error.localizedDescription);
    }];
    [operation start];
}

- (void)loadAllMedia {
        self.nextURL = [NSString stringWithFormat:getRecentMedia, appDelegate.accessToken];
}


@end
