//
//  Pagination.h
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Full response of Instagram requesy

@interface InstagramResponse : NSObject

@property (strong, nonatomic) NSString *nextURL;
@property (strong, nonatomic) NSString *nextMaxID;
@property (strong, nonatomic) NSArray *media;

@end
