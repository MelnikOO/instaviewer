//
//  DataLoader.h
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DataLoader;

@protocol DataLoaderDelegate <NSObject>

@required

- (void)dataLoader:(DataLoader *)aDataLoader didFinishLoadingMedia:(NSArray *)mediaArray;

@end

@interface DataLoader : NSObject

@property (strong, nonatomic) NSString *nextURL;
@property (weak, nonatomic) id<DataLoaderDelegate> delegate;

+ (id)sharedInstance;
- (void)loadAllMedia;

@end
