//
//  Like.h
//  InstaViewer
//
//  Created by Олег Мельник on 15.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Like

@interface Like : NSObject

@property (strong, nonatomic) NSString *username;

@end
