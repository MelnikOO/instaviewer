//
//  PhotoViewController.h
//  InstaViewer
//
//  Created by Олег Мельник on 14.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Media.h"

@interface PhotoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSString *photoURL;
@property NSInteger commentsCount;
@property (strong, nonatomic) Media *media;
@property (strong, nonatomic) UILabel *commentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *likedLabel;

@end
