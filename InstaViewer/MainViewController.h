//
//  ViewController.h
//  InstaViewer
//
//  Created by Олег Мельник on 14.08.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataLoader.h"

@interface MainViewController : UIViewController <UIWebViewDelegate, DataLoaderDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *prevButton;
- (IBAction)onPrevButtonTapped:(id)sender;
- (IBAction)onNextButtonTapped:(id)sender;
- (IBAction)onShareButtonTapped:(id)sender;

@end

